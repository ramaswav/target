import React, { Component } from 'react';
import TopNavigation from './components/topNavigation';
import './index.css';
import './App.css';

class App extends Component {

  render() {
    return (
          <div className="fixed-sn">
            <TopNavigation />
          </div>
    );
  }
}

export default App;
