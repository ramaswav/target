import React from 'react';
import Locations from './sections/Locations';

const DashboardPage =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <Locations />
      {/* <ChartSection2 /> */}
    </React.Fragment>
  )
}

export default DashboardPage;
