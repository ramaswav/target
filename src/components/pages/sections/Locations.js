import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge } from 'mdbreact';
class Locations extends Component {
  render(){
        return (
          <div className="col-12 main-content">
            {/* Stats */}
           <MDBCard className="mb-4 z-depth-3">
              <MDBCardHeader>
                 <h5>{this.props.locationData.Zipcode}_{this.props.locationData.City}_{this.props.locationData.id} <h6>({this.props.locationData.Address})</h6></h5>
              </MDBCardHeader>
              <MDBCardBody>
                 <MDBRow className="mb-12">
                    <MDBCol md="3" className="mb-4">
                       <MDBCard className="z-depth-3 population-card mb-4">
                          <MDBCardHeader>Population</MDBCardHeader>
                          <MDBCardBody className='cardVlist'>
                          Population
                          <br></br>
                          <div className='color-text'>{this.props.locationData.Population}</div>
                          People per sq. mile
                          <br></br>
                          <div className='color-text'>{this.props.locationData.People_per_sq_mile}</div>
                          Compared to USA
                          <br></br>
                          <div className='color-text'>Higher</div>
                          <div>+{this.props.locationData.Higher_1}</div>
                          <div>+{this.props.locationData.Higher_2}</div>
                          <div>+{this.props.locationData.Higher_3}</div>
                          <div className='color-text'>Lower</div>
                          <div>-{this.props.locationData.Lower_1}</div>
                          <div>-{this.props.locationData.Lower_2}</div>
                          <div>-{this.props.locationData.Lower_3}</div>
                          </MDBCardBody>

                       </MDBCard>
                    </MDBCol>
                    <MDBCol md="3" className="mb-4">
                       <MDBCard className="mb-4 z-depth-3">
                          <MDBCardHeader>Opportunity</MDBCardHeader>
                          <MDBCardBody className='cardVlist'>
                          <div>Categories(Target)</div>
                          <MDBBadge>{this.props.locationData.Categories_Target_1}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Target_2}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Target_3}</MDBBadge>&nbsp;
                          <br></br><div >Codes</div>
                          <MDBBadge>{this.props.locationData.Categories_Codes_1}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Codes_2}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Codes_3}</MDBBadge>&nbsp;

                          <br></br><br></br><div>Store Sales($):</div>
                          <h2>{!this.props.locationData.Store_sales_Dollars ? 0 : this.props.locationData.Store_sales_Dollars}</h2>

                          <br></br><div>Store Sales(U):</div>
                          <h2>{!this.props.locationData.Store_sales_Units ? 0 : this.props.locationData.Store_sales_Units}</h2>
                          </MDBCardBody>
                       </MDBCard>
                    </MDBCol>
                    <MDBCol md="6" className="mb-4">
                       <MDBCard className="mb-4 z-depth-3">
                          <MDBCardHeader>MAP</MDBCardHeader>
                          <MDBCardBody className='cardVlist'>
                          <iframe title="location gmap" className='resp-iframe' src={'https://maps.google.com/maps?q='+ this.props.locationData.Address +'&t=&z=13&ie=UTF8&iwloc=&output=embed'} frameborder="0"
                            style={{'border':'0'}} allowfullscreen></iframe>
                          </MDBCardBody>
                       </MDBCard>
                    </MDBCol>
                 </MDBRow>
              </MDBCardBody>
           </MDBCard>

           {/* demograph */}
           <MDBRow className="mb-12">
              <h2>Demographics</h2>
           </MDBRow>
           <MDBRow className="mb-12">
              <MDBCol md="4" className="mb-4">
                 <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                      <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[0]}/></a>
                    </MDBCardBody>
                 </MDBCard>
              </MDBCol>
              <MDBCol md="4" className="mb-4">
                 <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                    <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[1]}/></a>
                    </MDBCardBody>
                 </MDBCard>
              </MDBCol>
              <MDBCol md="4" className="mb-4">
                   <MDBCard className="mb-4 z-depth-3">
                      <MDBCardBody className='cardVlist'>
                      <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[2]}/></a>

                      </MDBCardBody>
                   </MDBCard>
                </MDBCol>
           </MDBRow>
           <MDBRow className="mb-12">
              <MDBCol md="4" className="mb-4">
                 <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                    <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[3]}/></a>
                    </MDBCardBody>
                 </MDBCard>
              </MDBCol>
              <MDBCol md="4" className="mb-4">
                 <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                    <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[4]}/></a>
                    </MDBCardBody>
                 </MDBCard>
              </MDBCol>
              <MDBCol md="4" className="mb-4">
                   <MDBCard className="mb-4 z-depth-3">
                      <MDBCardBody className='cardVlist'>
                        <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[5]}/></a>
                      </MDBCardBody>
                   </MDBCard>
                </MDBCol>
           </MDBRow>

           {/* Categories */}
           <MDBRow className="mb-12">
              <h2>Categories</h2>
           </MDBRow>
           <MDBRow className="mb-12 ">
              <MDBCol md="8" className="mb-4">
                 <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className="long-image-scroll cardVlist">
                      <div className="row col-md-12">
                        <div className="col-md-6"><a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.category_urls && this.props.locationData.category_urls[0]}/></a>
                        </div>
                        <div className="col-md-6"><a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.category_urls && this.props.locationData.category_urls[1]}/></a>
                        </div>
                      </div>
                      </MDBCardBody>
                 </MDBCard>
              </MDBCol>
              <MDBCol md="4" className="mb-4">
                   <MDBCard className="mb-4 z-depth-3 long-image-scroll cardVlist">
                      <MDBCardBody>
                      <a href="#!" className="logo-wrapper"><img className="cardVlist img-fluid" alt="Quilt AI" src={this.props.locationData.category_urls && this.props.locationData.category_urls[2]}/></a>
                      </MDBCardBody>
                   </MDBCard>
                </MDBCol>
           </MDBRow>

           {/* Codes/Tweets */}
           <MDBRow className="mb-12">
              <h2>Codes</h2>
           </MDBRow>
           <MDBRow className="mb-12">
              <MDBCol md="8" className="mb-4">
                 <MDBCard className="mb-4 z-depth-3 cardVlist">
                    <MDBCardBody className='long-image-scroll cardVlist'>
                      <img className="top-3-image img-fluid" alt="Quilt AI" src={this.props.locationData.social_urls && this.props.locationData.social_urls[0]}></img>
                    </MDBCardBody>
                 </MDBCard>
              </MDBCol>
              <MDBCol md="4" className="mb-4">
                   <MDBCard className="mb-4 z-depth-3 cardVlist">
                   <MDBCardHeader>
                      <h2>Top Tweets</h2>
                   </MDBCardHeader>
                      <MDBCardBody className='long-image-scroll cardVlist'>
                            <MDBListGroup style={{ height: "22px" }}>
                            { this.props.tweetsData !== undefined && Object.getOwnPropertyNames(this.props.tweetsData).length && this.props.tweetsData.social_text.map(function(tweet,index){
                              return <MDBListGroupItem key={index} href="#" >{tweet}</MDBListGroupItem>
                            })}
                            </MDBListGroup>
                      </MDBCardBody>
                   </MDBCard>
                </MDBCol>
           </MDBRow>
        </div>
        )
    }
}

export default Locations;
