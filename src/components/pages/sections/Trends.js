<MDBRow>
    <MDBCol className="d-flex justify-content-center" md='12'>
        <SubMenu />
    </MDBCol>
    <form className="col-md-12 filter-form-trend text-center border-4 z-depth-2 p-5 mb-4">
        <MDBRow>
            <MDBCol md="4" >
                <MDBSelect
                    search
                    color="secondary"
                    options={this.state.country.temp}
                    label="Country"
                    required
                />
            </MDBCol>
            <MDBCol md="4">
                <MDBSelect
                    search
                    multiple
                    color="secondary"
                    getTextContent={this.getValueOfSelectCategory}
                    getValue={this.setValueOfTrendOnCategory}
                    options={this.state.category.temp}
                    label="Category"
                />
            </MDBCol>
            <MDBCol md="4">
                <MDBSelect
                    search
                    multiple
                    color="secondary"
                    getTextContent={this.getValueOfSelectedTrend}
                    options={this.state.trend.temp}
                    label="Trend"
                />
            </MDBCol>
            <MDBCol md="4">

            </MDBCol>
            <MDBCol className="d-flex justify-content-center text-center" md="4">
                <MDBBtn onClick={this.renderResult.bind(this)} className="d-flex" size="md"> <MDBIcon far icon="clone" className="left" />Query</MDBBtn>
            </MDBCol>
            <MDBCol md="4">
            </MDBCol>
            {/* <MDBCol md='2'>
                                        Gender: {this.state.isLoading ? this.renderSpinner() : <RenderFilterOptions filter={this.state.gender.temp} />}
                                    </MDBCol> */}

        </MDBRow>
    </form>

    <div className="col-12 main-content">
        {/* Stats */}
        <MDBCard className="mb-4 z-depth-3">
            <MDBCardHeader>
                <h5>{this.props.locationData.Zipcode}_{this.props.locationData.City}_{this.props.locationData.id} <h6>({this.props.locationData.Address})</h6></h5>
            </MDBCardHeader>
            <MDBCardBody>
                <MDBRow className="mb-12">
                    <MDBCol md="3" className="mb-4">
                        <MDBCard className="z-depth-3 population-card mb-4">
                            <MDBCardHeader>Population</MDBCardHeader>
                            <MDBCardBody className='cardVlist'>
                                Trend: {this.state.isLoading ? this.renderSpinner() : <RenderFilterOptions filter={this.state.trend.temp} />}
                            </MDBCardBody>

                        </MDBCard>
                    </MDBCol>
                    <MDBCol md="3" className="mb-4">
                        <MDBCard className="mb-4 z-depth-3">
                            <MDBCardHeader>Opportunity</MDBCardHeader>
                            <MDBCardBody className='cardVlist'>
                                <div>Categories(Target)</div>
                                <MDBBadge>{this.props.locationData.Categories_Target_1}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Target_2}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Target_3}</MDBBadge>&nbsp;
                          <br></br><div >Codes</div>
                                <MDBBadge>{this.props.locationData.Categories_Codes_1}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Codes_2}</MDBBadge>&nbsp;
                          <MDBBadge>{this.props.locationData.Categories_Codes_3}</MDBBadge>&nbsp;
      
                          <br></br><br></br><div>Store Sales($):</div>
                                <h2>{!this.props.locationData.Store_sales_Dollars ? 0 : this.props.locationData.Store_sales_Dollars}</h2>

                                <br></br><div>Store Sales(U):</div>
                                <h2>{!this.props.locationData.Store_sales_Units ? 0 : this.props.locationData.Store_sales_Units}</h2>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                    <MDBCol md="6" className="mb-4">
                        <MDBCard className="mb-4 z-depth-3">
                            <MDBCardHeader>Demography</MDBCardHeader>
                            <MDBCardBody className='cardVlist'>
                            {(this.state.isLoading || this.isEmpty(this.state.demography_graph)) ? this.renderSpinner() : <Bar data={this.state.demography_graph}
                            options={{
                                legend: {
                                    display: true,
                                },
                                animation: {
                                    animateScale: true
                                },
                                title: {
                                    display: true,
                                    text: 'Age split of people looking for this'
                                }, scales: {
                                    xAxes: [{
                                        stacked: true,
                                        barThickness: 25,
                                    }],
                                    yAxes: [{
                                        stacked: true,
                                        barThickness: 25,
                                        scaleLabel: {
                                            display: true,
                                            labelString: "Average age"
                                        }
                                    },
                                    ]
                                },
                                plugins: {
                                    datalabels: {
                                        display: true,
                                        textAlign: "center",
                                        formatter: function (value, context) {
                                            return ''
                                        },
                                        color: 'black',
                                        font: {
                                            size: 5,
                                            weight: 1500,
                                            'font-family': 'Nunito',

                                        },
                                        offset: 4,
                                        padding: 435
                                    }
                                }
                            }} height={500} />}
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBCardBody>
        </MDBCard>

        {/* demograph */}
        <MDBRow className="mb-12">
            <h2>Demographics</h2>
        </MDBRow>
        <MDBRow className="mb-12">
            <MDBCol md="12" className="mb-4">
                <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                    <MDBRow>
                            <MDBCol md="4" >
                                <MDBSelect
                                    search
                                    selectAll
                                    multiple
                                    color="secondary"
                                    options={this.state.trendfilter.temp}
                                    label="Keywords"
                                    required
                                />
                            </MDBCol>
                            <MDBCol className="mt-4" md="3" >
                                <MDBBtn onClick={this.renderKeywordsCategory.bind(this)} className="d-flex" size="sm"> <MDBIcon far icon="clone" className="left" />Filter</MDBBtn>
                            </MDBCol>
                        </MDBRow>
                        {(this.state.isLoading || this.isEmpty(this.state.category_trend_graph)) ? this.renderSpinner() : <Bar data={this.state.category_trend_graph} options={{
                            maintainAspectRatio: true,
                            responsive: true,
                            type: 'bar',
                            legend: {
                                display: false,
                            },
                            animation: {
                                animateScale: true
                            },
                            title: {
                                display: true,
                                text: 'Trends'
                            }, scales: {
                                yAxes: [{
                                    barThickness: 25,
                                    scaleLabel: {
                                        display: true,
                                        labelString: "Percentage change"
                                    }
                                },
                                ]
                            },
                            plugins: {
                                datalabels: {
                                    display: true,
                                    textAlign: "center",
                                    formatter: function (value, context) {
                                        if (context.dataset.data[context.dataIndex] !== 0 || context.dataset.data[context.dataIndex] !== 0.00) {
                                            if (context.dataset.data[context.dataIndex] > 5 || context.dataset.label.length > 12) {
                                                return context.dataset.label
                                            }
                                            else {
                                                return ''
                                            }
                                        }
                                        else {
                                            return ''
                                        }
                                    },
                                    color: 'black',
                                    font: {
                                        size: 5,
                                        weight: 1500,
                                        'font-family': 'Nunito',

                                    },
                                    offset: 4,
                                    padding: 435
                                }
                            }
                        }} height={183} />}
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
        </MDBRow>
        <MDBRow className="mb-12">
            <MDBCol md="4" className="mb-4">
                <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                    <MDBRow>
                            <MDBCol md="4" >
                                <MDBSelect
                                    search
                                    selectAll
                                    multiple
                                    color="secondary"
                                    options={this.state.keywords.temp}
                                    label="Keywords"
                                    required
                                />
                            </MDBCol>
                            <MDBCol md="3" className="mt-4" >
                                <MDBBtn onClick={this.renderKeywordsTrends.bind(this)} className="d-flex" size="sm"> <MDBIcon far icon="clone" className="left" />Filter</MDBBtn>
                            </MDBCol>
                        </MDBRow>

                        {(this.state.isLoading || this.isEmpty(this.state.keyword_trend_graph)) ? this.renderSpinner() : <Bar data={this.state.keyword_trend_graph} options={{
                            maintainAspectRatio: true,
                            responsive: true,
                            fill: false,
                            type: 'bar',
                            bezierCurve: false,
                            elements: {
                                line: {
                                    tension: 0
                                }
                            },
                            legend: {
                                display: false,
                            },
                            animation: {
                                animateScale: true
                            },
                            title: {
                                display: true,
                                text: 'Keywords in trends'
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: true,
                                        barThickness: 95,
                                    }
                                }],
                                yAxes: [{
                                    gridLines: {
                                        display: true,
                                        barThickness: 95,
                                    },
                                    scaleLabel: {
                                        display: true,
                                        labelString: "Percentage change"
                                    }
                                }]
                            },
                            plugins: {
                                datalabels: {
                                    display: true,
                                    textAlign: "center",
                                    formatter: function (value, context) {
                                        if (context.dataset.data[context.dataIndex] !== 0 || context.dataset.data[context.dataIndex] !== 0.00) {
                                            if (context.dataset.data[context.dataIndex] > 5 || context.dataset.label.length > 12) {
                                                return context.dataset.label
                                            }
                                            else {
                                                return ''
                                            }
                                        }
                                        else {
                                            return ''
                                        }
                                    },
                                    color: 'black',
                                    font: {
                                        size: 5,
                                        weight: 1500,
                                        'font-family': 'Nunito',

                                    },
                                    offset: 4,
                                    padding: 435
                                }
                            }

                            // scales: {
                            //     xAxes: [{
                            //         stacked: true,
                            //         barThickness: 95,
                            //     }],
                            //     yAxes: [{
                            //         stacked: false,
                            //         barThickness: 95,
                            //     }]
                            // }

                        }} height={183} />}
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
            <MDBCol md="4" className="mb-4">
                <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                    <MDBRow>
                            <MDBCol md="4" >
                                <MDBSelect
                                    search
                                    selectAll
                                    multiple
                                    color="secondary"
                                    options={this.state.keywords.temp}
                                    label="Keywords"
                                    required
                                />
                            </MDBCol>
                            <MDBCol className="mt-4" md="3" >
                                <MDBBtn onClick={this.renderKeywordsSearch.bind(this)} className="d-flex" size="sm"> <MDBIcon far icon="clone" className="left" />Filter</MDBBtn>
                            </MDBCol>
                        </MDBRow>

                        {(this.state.isLoading || this.isEmpty(this.state.keyword_search_trends_graph)) ? this.renderSpinner() : <Line data={this.state.keyword_search_trends_graph}
                            options={{
                                legend: {
                                    display: this.state.keyword_search_trends_graph.datasets.length <= 30,
                                    fontSize: 8,
                                    position: 'right',
                                    padding: 1,
                                    boxWidth: 10,
                                    labels: {
                                        usePointStyle: true
                                    }
                                },
                                scales: {
                                    xAxes: [{
                                        gridLines: {
                                            display: true,
                                            barThickness: 95,
                                        }
                                    }],
                                    yAxes: [{
                                        gridLines: {
                                            display: true,
                                            barThickness: 95,
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: "Search Volume"
                                        }
                                    }]
                                },
                                plugins: {
                                    datalabels: {
                                        display: this.state.keyword_search_trends_graph.datasets.length >= 30,
                                        textAlign: "center",
                                        formatter: function (value, context) {
                                            if (context.dataset.data[context.dataIndex] !== 0 || context.dataset.data[context.dataIndex] !== 0.00) {
                                                if (context.dataset.data[context.dataIndex] > 500) {
                                                    return context.dataset.label
                                                }
                                                else {
                                                    return ''
                                                }
                                            }
                                            else {
                                                return ''
                                            }
                                        },
                                        color: 'black',
                                        font: {
                                            size: 5,
                                            weight: 1500,
                                            'font-family': 'Nunito',
                                        },
                                        offset: 4,
                                        padding: 135
                                    }
                                }
                            }} height={183} />}
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
            <MDBCol md="4" className="mb-4">
                <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className='cardVlist'>
                        <a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.demographic_urls && this.props.locationData.demographic_urls[5]} /></a>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
        </MDBRow>

        {/* Categories */}
        <MDBRow className="mb-12">
            <h2>Categories</h2>
        </MDBRow>
        <MDBRow className="mb-12 ">
            <MDBCol md="8" className="mb-4">
                <MDBCard className="mb-4 z-depth-3">
                    <MDBCardBody className="long-image-scroll cardVlist">
                        <div className="row col-md-12">
                            <div className="col-md-6"><a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.category_urls && this.props.locationData.category_urls[0]} /></a>
                            </div>
                            <div className="col-md-6"><a href="#!" className="logo-wrapper"><img className="img-fluid" alt="Quilt AI" src={this.props.locationData.category_urls && this.props.locationData.category_urls[1]} /></a>
                            </div>
                        </div>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
            <MDBCol md="4" className="mb-4">
                <MDBCard className="mb-4 z-depth-3 long-image-scroll cardVlist">
                    <MDBCardBody>
                        <a href="#!" className="logo-wrapper"><img className="cardVlist img-fluid" alt="Quilt AI" src={this.props.locationData.category_urls && this.props.locationData.category_urls[2]} /></a>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
        </MDBRow>

        {/* Codes/Tweets */}
        <MDBRow className="mb-12">
            <h2>Codes</h2>
        </MDBRow>
        <MDBRow className="mb-12">
            <MDBCol md="8" className="mb-4">
                <MDBCard className="mb-4 z-depth-3 cardVlist">
                    <MDBCardBody className='long-image-scroll cardVlist'>
                        <img className="top-3-image img-fluid" alt="Quilt AI" src={this.props.locationData.social_urls && this.props.locationData.social_urls[0]}></img>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
            <MDBCol md="4" className="mb-4">
                <MDBCard className="mb-4 z-depth-3 cardVlist">
                    <MDBCardHeader>
                        <h2>Top Tweets</h2>
                    </MDBCardHeader>
                    <MDBCardBody className='long-image-scroll cardVlist'>
                        <MDBListGroup style={{ height: "22px" }}>
                            {this.props.tweetsData !== undefined && Object.getOwnPropertyNames(this.props.tweetsData).length && this.props.tweetsData.social_text.map(function (tweet, index) {
                                return <MDBListGroupItem key={index} href="#" >{tweet}</MDBListGroupItem>
                            })}
                        </MDBListGroup>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
        </MDBRow>
    </div>
    

</MDBRow>