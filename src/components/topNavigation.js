import React, { Component } from 'react';
import logo from "../assets/logo-white.svg";
import { MDBFormInline, MDBNavbar } from 'mdbreact';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import axios from 'axios';
import Locations from './pages/sections/Locations';

class TopNavigation extends Component {
    state = {
      location: [],
      currentLocation:{},
      tweetsData:[],
      currentTweetsData:{}
    }
    onClick = () => {
        this.setState({
            collapse: !this.state.collapse,
        });
    }
    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    getData(){
      axios.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/location.json')
      .then(response =>{
        this.setState({location:response.data,currentLocation:response.data[0]})
      });

      axios.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/tweets.json')
      .then(response =>{
        console.log(response.data);
        this.setState({tweetsData:response.data})
      });

    }

    handleChange(event) {
        let foundValue=[]
        if ( this.state.tweetsData ) {
          foundValue = this.state.tweetsData.filter(obj=>obj.id===this.state.location[event.target.value].id);
          this.setState({currentTweetsData:foundValue[0]});
        }
        else {
          this.setState({currentTweetsData:{}});
        }
        this.setState({currentLocation:this.state.location[event.target.value]});
    }

    componentDidMount(){
      this.getData();
      // setInterval(function() {
      //   this.getData();
      // }.bind(this), 10000);
    }

    render() {

        return (
          <div className="row col-12">
             <MDBNavbar className="row col-12">
                <div className="col-sm-2 text-left"><img width={100} className="img-responsive img-fluid" alt="Quilt AI" src={logo}></img></div>
                <div className="col-1">&nbsp;</div>
                <div className="col-md-12 mr-auto text-center"><h2>Target Dashboard</h2></div>
             </MDBNavbar>
             <MDBFormInline className="row select-form col-md-12 md-form mr-auto m-0">
                  <div className='selects-in-line mr-auto row col-md-12'>
                    <select className="col-md-2 custom-select select-options" defaultValue={0}>
                      <option value="location">Location</option>
                    </select>
                    <select className="col-md-2 custom-select select-options" defaultValue={0} onChange={this.handleChange.bind(this)}>
                    {this.state.location.map(function(locationobject, index){
                      return <option key={locationobject.id} value={index}>{locationobject.City}-{locationobject.Zipcode}</option>;
                    })}
                    </select>
                    {/*<div className="col-md-2 col-sm-12 align-middle">
                      <MDBBtn className="btn-submit-location" size="sm">Let's Go</MDBBtn>
                    </div>*/}
                 </div>
             </MDBFormInline>
             <div className="col-12">&nbsp;</div>
             <Locations locationData={this.state.currentLocation} tweetsData={this.state.currentTweetsData}/>
          </div>
        );
    }
}

export default TopNavigation;
